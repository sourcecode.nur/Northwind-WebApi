﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;  
using System.Security.Cryptography;
using System.Linq.Expressions;
using System.Reflection;

namespace NorthWind.Common
{
    public class Commons
    {
        public static string DEFAULT_FORMAT_DATE = "dd-MMM-yyyy";

        public static int GetInt(object obj)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static DateTime GetDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception ex) { }
            return DateTime.Now;
        }

        public static double GetDouble(object obj)
        {
            try
            {
                return Convert.ToDouble(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static decimal GetDecimal(object obj)
        {
            try
            {
                return Convert.ToDecimal(obj);
            }
            catch (Exception ex) { }
            return 0;
        }  

        public static string GetImageUrl(byte[] bytes)
        {
            //Cuma bisa untuk image di NorthWind
            if (bytes.Length >=78)
            {
                return "data:image/png;base64," + Convert.ToBase64String(bytes, 78, bytes.Length - 78);
            }

            return "";
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString().ToUpper();
        }

        public static string GetNewSessionID()
        {
            return Guid.NewGuid().ToString().Replace("-","");
        }

        public static PropertyInfo GetPropertyInfo<TSource, TProperty>(
            TSource source,
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
                Type type = typeof(TSource);

                MemberExpression member = propertyLambda.Body as MemberExpression;

            if (member == null)
            {
                throw new ArgumentException(
                    String.Format(
                        "Expression '{0}' refers to a method, not a property.",
                        propertyLambda.ToString()));
            }

            var propInfo = member.Member as PropertyInfo;

            if (propInfo == null)
            {
                throw new ArgumentException(
                    String.Format(
                        "Expression '{0}' refers to a field, not a property.",
                        propertyLambda.ToString()));
            }

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
            {
                throw new ArgumentException(
                    String.Format(
                       "Expresion '{0}' refers to a property that is not from type {1}.",
                       propertyLambda.ToString(),
                       type));
            }

            return propInfo;
        }

        public static String GetPropertyName<T>(
            Expression<Func<T>> propertyLambda)
        {
            var me = propertyLambda.Body as MemberExpression;

            if (me == null)
            {
                throw new ArgumentException(@"You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");
            }

            return me.Member.Name;
        }
    }
}
