Git global setup

git config --global user.name "Muhammad Nur"
git config --global user.email "nur@code.id"

Create a new repository

git clone https://gitlab.com/nur1/Northwind-MVC.git
cd Northwind-MVC
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/nur1/Northwind-MVC.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/nur1/Northwind-MVC.git
git push -u origin --all
git push -u origin --tags