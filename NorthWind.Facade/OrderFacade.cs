﻿using NorthWind.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NorthWind.Facade
{
    public class OrderFacade : BaseCRUD<Order>
    {
        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT [OrderID]
                                    ,[CustomerID]
                                    ,[EmployeeID]
                                    ,Convert(varchar(12),[OrderDate],106)[OrderDate]
                                    ,[RequiredDate]
                                    ,[ShippedDate]
                                    ,[ShipVia]
                                    ,[Freight]
                                    ,[ShipName]
                                    ,[ShipAddress]
                                    ,[ShipCity]
                                    ,[ShipRegion]
                                    ,[ShipPostalCode]
                                    ,[ShipCountry]
                                FROM [dbo].[Orders] WHERE " + criteria;  
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public object GetOrderModel()
        {
            throw new NotImplementedException();
        }

        public void DataInsert(Order oOrder, List<OrderDetailMap> listOrderDetail, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using(var scope = DB.GetTransaction())
                {
                    DB.Insert(oOrder);
                    listOrderDetail.ForEach((o)=> 
                    {
                        var detail = new OrderDetail() {
                            OrderID = oOrder.OrderID, ProductID = o.ProductID,
                            UnitPrice = o.UnitPrice, Quantity = o.Quantity, Discount = o.Discount};
                        DB.Insert("Order Details", "OrderID", false, detail); 
                    });
                    scope.Complete();
                }
            }catch(Exception ex)
            {
                errorMsg = ex.Message.Replace("'", "");
            }
        }

        public void DataUpdate(Order oOrder, List<OrderDetailMap> listOrderDetail, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    DB.Update(oOrder);
                    listOrderDetail.ForEach((o) =>
                    {
                        var detail = new OrderDetail()
                        {
                            OrderID = oOrder.OrderID,
                            ProductID = o.ProductID,
                            UnitPrice = o.UnitPrice,
                            Quantity = o.Quantity,
                            Discount = o.Discount
                        };
                        
                        if (DB.SingleOrDefault<OrderDetail>(new Sql().Where("OrderID = @0 AND ProductID = @1", detail.OrderID, detail.ProductID)) == null)
                        { 
                            DB.Insert("Order Details", "OrderID", false, detail);                            
                        }
                        else
                        {
                            DB.Execute("UPDATE [Order Details] SET Quantity = @2, Discount = @3 WHERE OrderID = @0 AND ProductID = @1", 
                                detail.OrderID, detail.ProductID, detail.Quantity, detail.Discount);
                        }
                    });

                    var list = DB.Fetch<OrderDetail>(new Sql().Where("OrderID = @0", oOrder.OrderID));
                    list.ForEach((o)=>
                    {
                        if(listOrderDetail.SingleOrDefault<OrderDetailMap>(x => x.OrderID == o.OrderID && x.ProductID == o.ProductID) == null)
                        {
                            DB.Delete<OrderDetail>(new Sql().Where("OrderID = @0 AND ProductID = @1", o.OrderID, o.ProductID));
                        }
                    });
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.Replace("'", "");
            }
        }

        public void DataDelete(Order oOrder, out string errorMsg)
        {
            errorMsg = ""; 
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    DB.Delete<OrderDetail>(new Sql().Where("OrderID = @0", oOrder.OrderID));
                    DB.Delete(oOrder); 
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message.Replace("'", "");
            }
        }

        public new Page<OrdersView> PageDataForDataTables(string criteriaValue, int page, int perPage, string orderBy)
        {
            string columns = @"[OrderID],[CustomerName],[EmployeeName],[OrderDate],[RequiredDate],[ShippedDate],[ShipperName],[Freight],[ShipName],[ShipAddress],[ShipCity],[ShipCountry]";
            var cols = columns.Split(',');
            string criteria = string.Empty;
            foreach (var col in cols)
            {
                criteria += string.IsNullOrEmpty(criteria) ? "" : "OR";
                criteria += string.Format(" {0} LIKE @0 ", col);
            }
            criteria = string.IsNullOrEmpty(criteria) ? " 1=1 " : criteria;
            string sql = @"SELECT * FROM (
                        SELECT [OrderID]
                              ,a.[CustomerID]
	                          ,b.CompanyName [CustomerName]
                              ,a.[EmployeeID]
	                          ,(c.FirstName +' '+ c.LastName) [EmployeeName]
                              ,[OrderDate]
                              ,[RequiredDate]
                              ,[ShippedDate]
                              ,[ShipVia]
	                          ,d.CompanyName[ShipperName]
                              ,[Freight]
                              ,[ShipName]
                              ,[ShipAddress]
                              ,[ShipCity]
                              ,[ShipRegion]
                              ,[ShipPostalCode]
                              ,[ShipCountry]
                          FROM [dbo].[Orders] a
                          INNER JOIN [dbo].[Customers] b ON a.CustomerID = b.CustomerID
                          INNER JOIN [dbo].[Employees] c ON c.EmployeeID = a.EmployeeID
                          INNER JOIN [dbo].[Shippers] d ON d.ShipperID = a.ShipVia
                        )x WHERE " + criteria;
            criteriaValue = string.Format("%{0}%", criteriaValue);
            return DB.Page<OrdersView>(page, perPage, new Sql(sql, criteriaValue).OrderBy(orderBy));
        }

        public OrderModel GetOrderModel(int OrderID)
        {
            OrderModel order = new OrderModel();
            string sql = @"SELECT a.* ,b.* ,c.* ,d.*
                      FROM [dbo].[Orders] a
                      INNER JOIN [dbo].[Customers] b ON b.CustomerID = a.CustomerID
                      INNER JOIN [dbo].[Employees] c ON c.[EmployeeID] = a.[EmployeeID]
                      INNER JOIN [dbo].[Shippers] d ON d.ShipperID = a.[ShipVia]
                      WHERE [OrderID] = @0

                      SELECT a.* ,[ProductName] 
                    FROM [Order Details] a
                    INNER JOIN [Products] b ON b.[ProductID]=a.[ProductID]
                    WHERE [OrderID] = @0";


            var queries = DB.QueryMultiple(new Sql(sql, OrderID));
            order = queries.Read<Order, Customers, Employees, Shippers, OrderModel>((o, c, e, s) => {
                OrderModel model = new OrderModel();
                model.Order = o;
                model.Customer = c;
                model.Employee = e;
                model.Shipper = s;
                return model;
            }).SingleOrDefault();

            order.ListOrderDetail = queries.Read<OrderDetailMap>().ToList();
            return order;
        }

    }

    public class OrderModel
    {
        public Order Order { get; set; }

        public Customers Customer { get; set; }

        public Employees Employee { get; set; }

        public Shippers Shipper { get; set; }

        public List<OrderDetailMap> ListOrderDetail { get; set; }
    }
}
