﻿using NorthWind.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace NorthWind.Facade
{ 
    public class ProductsFacade : BaseCRUD<Products>
    {
        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM (
                        SELECT [ProductID]
                              ,[ProductName]
                              ,a.[SupplierID]
	                          ,[CompanyName]
                              ,a.[CategoryID]
	                          ,[CategoryName]
                              ,[QuantityPerUnit]
                              ,[UnitPrice]
                              ,[UnitsInStock]
                              ,[UnitsOnOrder]
                              ,[ReorderLevel]
                              ,[Discontinued]
                          FROM [dbo].[Products] a
                          INNER JOIN [dbo].[Suppliers] b ON b.SupplierID = a.SupplierID
                          INNER JOIN [dbo].[Categories] c ON c.CategoryID = a.CategoryID
                        )x WHERE " + criteria;  
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public new Page<ProductVIew> PageDataForDataTables(string criteriaValue, int page, int perPage, string orderBy)
        {
            string columns = @"[ProductID],[ProductName],[SupplierID],[CompanyName],[CategoryID],[CategoryName]" +
                        ",[QuantityPerUnit],[UnitPrice],[UnitsInStock],[UnitsOnOrder],[ReorderLevel],[Discontinued]";
            var cols = columns.Split(',');
            string criteria = string.Empty;
            foreach (var col in cols)
            {
                criteria += string.IsNullOrEmpty(criteria) ? "" : "OR";
                criteria += string.Format(" {0} LIKE @0 ", col); 
            }
            criteria = string.IsNullOrEmpty(criteria) ? " 1=1 " : criteria;
            string sql = @"SELECT * FROM (
                        SELECT [ProductID]
                              ,[ProductName]
                              ,a.[SupplierID]
	                          ,[CompanyName]
                              ,a.[CategoryID]
	                          ,[CategoryName]
                              ,[QuantityPerUnit]
                              ,[UnitPrice]
                              ,[UnitsInStock]
                              ,[UnitsOnOrder]
                              ,[ReorderLevel]
                              ,[Discontinued]
                          FROM [dbo].[Products] a
                          INNER JOIN [dbo].[Suppliers] b ON b.SupplierID = a.SupplierID
                          INNER JOIN [dbo].[Categories] c ON c.CategoryID = a.CategoryID
                        )x WHERE " + criteria;
            criteriaValue = string.Format("%{0}%", criteriaValue);
            return DB.Page<ProductVIew>(page, perPage, new Sql(sql, criteriaValue).OrderBy(orderBy));
        }
    }
}
