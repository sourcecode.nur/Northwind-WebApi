﻿using NorthWind.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NorthWind.Facade
{
    public class EmployeeFacade : BaseCRUD<Employees>
    { 
        public new void Update(Employees emp)
        {
            List<string> columns = new List<string>();

            DB.Update(emp, columns);
        }
    }
}
