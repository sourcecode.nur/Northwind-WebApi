﻿using NorthWind.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NorthWind.Facade
{
    public class CustomersFacade : BaseCRUD<Customers>
    {
        public new void Insert(Customers obj)
        {
            DB.Insert("Customers", "CustomerID", false, obj);
        }

        public new bool DataInsert(Customers cus, out string errorMsg)
        { 
            errorMsg = "";
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(cus);
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }


        public bool DataDelete(string id, out string errorMsg)
        { 
            errorMsg = "";
            try
            {
                var cus = GetItemByID(id);
                if (cus == null) throw new Exception("Data not found!");
                using (var scope = DB.GetTransaction())
                {
                    DB.Delete<Customers>(new Sql().Where("CustomerID = @0", id));
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }

    }
}
