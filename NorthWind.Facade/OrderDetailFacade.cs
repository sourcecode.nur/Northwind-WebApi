﻿using NorthWind.Entity;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NorthWind.Facade
{
    public class OrderDetailFacade : BaseCRUD<OrderDetail>
    {
        public Page PageDataByCriteria( string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM(
                                    SELECT [OrderID],
                                    a.[ProductID],
                                    a.[UnitPrice],
                                    b.[ProductName],
                                    [Quantity],
                                    [Discount]
                                    FROM [dbo].[Order Details] a
                                    INNER JOIN [dbo].[Products] b ON b.ProductID = a.ProductID)X
                                    WHERE " + criteria;  
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public DataTable ProductListTable()
        {

            DataTable table = new DataTable();
            table.Columns.Add("ProductID", typeof(int));
            table.Columns.Add("ProductName", typeof(string));
            table.Columns.Add("UnitPrice", typeof(decimal));
            table.Columns.Add("Quantity", typeof(int));
            table.Columns.Add("Discount", typeof(int));
            return table;
        }

        public List<OrderDetailMap> GetListOrderDetailMap(int orderID)
        {
            string sql = @"SELECT 
                        [OrderID]
                        ,a.[ProductID]
                        ,[ProductName]
                        ,a.[UnitPrice]
                        ,[Quantity]
                        ,[Discount]
                        FROM [Order Details] a
                        INNER JOIN [Products] b ON b.[ProductID]=a.[ProductID]";
            return DB.Fetch<OrderDetailMap>(new Sql(sql).Where("OrderID = @0", orderID));
        }
    }
}
