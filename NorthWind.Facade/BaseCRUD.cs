﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco; 
using System.Data;

namespace NorthWind.Facade
{
    public class BaseCRUD<T>
    {
        public Database DB;

        public BaseCRUD()
        {
            DB = new Database("NORTHWIND");            
        }

        public object Insert(T obj)
        {
            return DB.Insert(obj); 
        }

        public void Update(T obj)
        {
            DB.Update(obj);
        }

        public void Delete(T obj)
        {
            DB.Delete(obj);
        }

        public void DeleteByID(int id)
        {
            DB.Delete<T>(id);
        }
         
        public bool DataInsert(T obj, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(obj);
                    scope.Complete();
                }
            }
            catch (Exception ex) {
                errorMsg = ex.Message.Replace("'", "");                                
                return false;
            }
            return true;
        }

        public bool DataUpdate(object id, T obj, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                var item = GetItemByID(id);
                if (item == null) throw new Exception("Data not found!");
                using (var scope = DB.GetTransaction())
                {
                    Update(obj);
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }

        public bool DataDelete(object id, out string errorMsg)
        {
            errorMsg = "";
            try
            {
                var item = GetItemByID(id);
                if (item == null) throw new Exception("Data not found!");
                using (var scope = DB.GetTransaction())
                {
                    DB.Delete<T>(item);
                    scope.Complete();
                }
            }
            catch (Exception ex) { errorMsg = ex.Message.Replace("'", ""); return false; }
            return true;
        }

        public T GetItemByID(object id)
        {
            return DB.SingleOrDefault<T>(id);
        }

        public List<T> GetAllItems()
        {           
            return DB.Fetch<T>(new Sql().Where("1=1"));
        }

        public List<T> GetListItemsByCriteria(int page, int pageSize, string criteria, string orderClause)
        {
            return DB.Fetch<T>(page, pageSize, new Sql().Where(criteria).OrderBy(new string[] { orderClause}));
        }

        public List<T> GetListItemsByCriteria(int page, int pageSize, string criteria, object[] param, string orderClause)
        {
            return DB.Fetch<T>(page, pageSize, new Sql().Where(criteria, param).OrderBy(new string[] { orderClause }));
        }

        public int GetListItemsByCriteriaCount(string criteria, params object[] param)
        {            
            return DB.FetchCount<T>(new Sql().Where(criteria, param));
        } 

        public List<T> GetListItemsByCriteria(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            return DB.Fetch<T>(new Sql().Where(sql, cv));
        }

        public List<T> GetListItemsByCriteria(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            var list = DB.Fetch<T>(new Sql().Where(sql, cv).OrderBy(orderBy));
            list = DB.Page<T>(page, itemsPerPage, new Sql().Where(sql, cv).OrderBy(orderBy)).Items;
            return list;
        }

        public int GetListItemsByCriteriaCount(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            return DB.FetchCount<T>(new Sql().Where(sql, cv)); 
        }

        public List<T> GetListItemsByLikeCriteria(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} LIKE @{1} ", key, i);
                cv[i] = string.Format("%{0}%", param[key]);
                i++;
            }
            var list = DB.Fetch<T>(new Sql().Where(sql, cv).OrderBy(orderBy));
            list = DB.Page<T>(page, itemsPerPage, new Sql().Where(sql, cv).OrderBy(orderBy)).Items;
            return list;
        }

        public int GetListItemsByLikeCriteriaCount(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} LIKE @{1} ", key, i);
                cv[i] = string.Format("%{0}%", param[key]);
                i++;
            }
            return DB.FetchCount<T>(new Sql().Where(sql, cv));
        }

        public DataTable GetListItemsAsDataTable()
        {
            return DB.FetchDataTable<T>(new Sql().Where("1=1"));
        }

        public DataTable GetListItemsAsDataTable(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            return DB.FetchDataTable<T>(new Sql().Where(sql, cv));
        }

        public DataTable GetListItemsAsDataTable(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] cv = new object[param.Count]; 
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                cv[i] = param[key];
                i++;
            }
            return DB.PageDataTable<T>(page, itemsPerPage, new Sql().Where(sql, cv).OrderBy(orderBy));
        }

        public T GetItemByCriteria(string column, object value)
        {
            return DB.SingleOrDefault<T>(new Sql().Where(string.Format(" {0} = @0", column), value)); 

        }

        public Page<T> PageDataForDataTables(string crtVal, int page, int length, string strOrder)
        {
            var props = typeof(T).GetProperties();
            string sql = string.Empty;
            int i = 0;
            object[] cv = new object[props.Length];
            foreach (var prop in props)
            {
                if (prop.PropertyType.Name.Contains("Byte"))
                {
                    continue;
                }
                sql += string.IsNullOrEmpty(sql) ? "" : "OR";
                sql += string.Format(" {0} LIKE @{1} ", prop.Name, i);
                cv[i] = string.Format("%{0}%", crtVal);
                i++;
            }
            sql = string.IsNullOrEmpty(sql) ? " 1=1 " : sql;
            return DB.Page<T>(page, length, new Sql().Where(sql, cv).OrderBy(strOrder));
        }

    }
}
