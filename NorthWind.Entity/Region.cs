﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NorthWind.Entity
{
    [TableName("Region")]
    [PrimaryKey("RegionID")] 
    public class Region
    {
        public int RegionID { get; set; }
        public string RegionDescription { get; set; }
    }
}
