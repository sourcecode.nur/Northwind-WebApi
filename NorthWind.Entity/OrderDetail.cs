﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWind.Entity
{
    [TableName("Order Details")]
    [PrimaryKey("OrderID,ProductID")] 
    public class OrderDetail
    {
        [PetaPoco.Column]
        public int OrderID { get; set; }

        [PetaPoco.Column]
        public int ProductID { get; set; }

        [PetaPoco.Column]
        public decimal UnitPrice { get; set; }

        [PetaPoco.Column]
        public int Quantity { get; set; }

        [PetaPoco.Column]
        public Single Discount { get; set; }
      
    }
}
