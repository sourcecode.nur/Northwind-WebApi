﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWind.Entity
{
    public class Territories
    {
        public string TerritoryID { get; set; }
        public string TerritoryDescription { get; set; }
        public int RegionID { get; set; }
    }
}
