﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWind.Entity
{
    [TableName("Orders")]
    [PrimaryKey("OrderID")]
    [ExplicitColumns]  
    public class Order
    {
        [PetaPoco.Column]
        public int OrderID { get; set; } 
        [PetaPoco.Column]
        public string CustomerID { get; set; }
        [PetaPoco.Column]
        public int? EmployeeID { get; set; }
        [PetaPoco.Column]
        public DateTime OrderDate { get; set; }
        [PetaPoco.Column]
        public DateTime RequiredDate { get; set; }
        [PetaPoco.Column]
        public DateTime ShippedDate { get; set; }
        [PetaPoco.Column]
        public int? ShipVia { get; set; }
        [PetaPoco.Column]
        public decimal Freight { get; set; }
        [PetaPoco.Column]
        public string ShipName { get; set; }
        [PetaPoco.Column]
        public string ShipAddress { get; set; }
        [PetaPoco.Column]
        public string ShipCity { get; set; }
        [PetaPoco.Column]
        public string ShipRegion { get; set; }
        [PetaPoco.Column]
        public string ShipPostalCode { get; set; } 
        [PetaPoco.Column]
        public string ShipCountry { get; set; }
    }

    public class OrdersView
    { 
        public int OrderID { get; set; } 
        public string CustomerID { get; set; } 
        public string CustomerName { get; set; }
        public int EmployeeID { get; set; } 
        public string EmployeeName { get; set; }
        public DateTime OrderDate { get; set; } 
        public DateTime RequiredDate { get; set; } 
        public DateTime ShippedDate { get; set; } 
        public int ShipVia { get; set; }
        public string ShipperName { get; set; }
        public decimal Freight { get; set; } 
        public string ShipName { get; set; } 
        public string ShipAddress { get; set; } 
        public string ShipCity { get; set; } 
        public string ShipRegion { get; set; } 
        public string ShipPostalCode { get; set; } 
        public string ShipCountry { get; set; }
    }
}
