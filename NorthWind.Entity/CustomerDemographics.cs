﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWind.Entity
{
    [TableName("CustomerDemographics")]
    [PrimaryKey("CustomerTypeID")]
    [Serializable]
    public class CustomerDemographics
    {
        public string CustomerTypeID { get; set; }

        public string CustomerDesc { get; set; }
    }
}
