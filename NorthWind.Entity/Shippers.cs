﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NorthWind.Entity
{
    [TableName("Shippers")]
    [PrimaryKey("ShipperID")] 
    public class Shippers
    {
        public int ShipperID { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
    }
}
