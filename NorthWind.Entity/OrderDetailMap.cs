﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NorthWind.Entity
{  
    public class OrderDetailMap
    {
        [PetaPoco.Column]
        public int OrderID { get; set; }

        [PetaPoco.Column]
        public int ProductID { get; set; }

        [PetaPoco.Column]
        public string ProductName { get; set; }

        [PetaPoco.Column]
        public decimal UnitPrice { get; set; }

        [PetaPoco.Column]
        public int Quantity { get; set; }

        [PetaPoco.Column]
        public Single Discount { get; set; }

    }
}

