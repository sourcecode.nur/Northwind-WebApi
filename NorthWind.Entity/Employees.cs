﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NorthWind.Entity
{
    [PetaPoco.TableName("Employees")]
    [PetaPoco.PrimaryKey("EmployeeID")]
    [PetaPoco.ExplicitColumns] 
    public class Employees
    {
        [PetaPoco.Column]
        public int EmployeeID { get; set; }

        [PetaPoco.Column]
        public string LastName { get; set; }

        [PetaPoco.Column]
        public string FirstName { get; set; }

        [PetaPoco.Column]
        public string Title { get; set; }

        [PetaPoco.Column]
        public string TitleOfCourtesy { get; set; }

        [PetaPoco.Column]
        public DateTime BirthDate { get; set; }

        [PetaPoco.Column]
        public DateTime HireDate { get; set; }

        [PetaPoco.Column]
        public string Address { get; set; }

        [PetaPoco.Column]
        public string City { get; set; }

        [PetaPoco.Column]
        public string Region { get; set; }

        [PetaPoco.Column]
        public string PostalCode { get; set; }

        [PetaPoco.Column]
        public string Country { get; set; }

        [PetaPoco.Column]
        public string HomePhone { get; set; }

        [PetaPoco.Column]
        public string Extension { get; set; }

        [PetaPoco.Column]
        public byte[] Photo { get; set; }

        [PetaPoco.Column]
        public string Notes { get; set; }

        [PetaPoco.Column]
        public int? ReportsTo { get; set; }

        [PetaPoco.Column]
        public string PhotoPath { get; set; }

        //[PetaPoco.Column]
        //public string UserID { get; set; }

        //[PetaPoco.Column]
        //public string Password { get; set; }

    }
}
