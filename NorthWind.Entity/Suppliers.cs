﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWind.Entity
{
    [PetaPoco.TableName("Suppliers")]
    [PetaPoco.PrimaryKey("SupplierID")]
    [PetaPoco.ExplicitColumns] 
    public class Suppliers
    {
        [PetaPoco.Column]
        public int SupplierID { get; set; }

        [PetaPoco.Column]
        public string CompanyName { get; set; }

        [PetaPoco.Column]
        public string ContactName { get; set; }

        [PetaPoco.Column]
        public string ContactTitle { get; set; }

        [PetaPoco.Column]
        public string Address { get; set; }

        [PetaPoco.Column]
        public string City { get; set; }

        [PetaPoco.Column]
        public string Region { get; set; }

        [PetaPoco.Column]
        public string PostalCode { get; set; }

        [PetaPoco.Column]
        public string Country { get; set; }

        [PetaPoco.Column]
        public string Phone { get; set; }

        [PetaPoco.Column]
        public string Fax { get; set; }

        [PetaPoco.Column]
        public string HomePage { get; set; }

    }
}
