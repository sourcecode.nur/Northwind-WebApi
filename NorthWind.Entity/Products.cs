﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;

namespace NorthWind.Entity
{
    [TableName("Products")]
    [PrimaryKey("ProductID")] 
    [ExplicitColumns]
    public class Products
    {
        [PetaPoco.Column]
        public int ProductID { get; set; }

        [PetaPoco.Column]
        public string ProductName { get; set; }

        [PetaPoco.Column]
        public int SupplierID { get; set; }

        [PetaPoco.Column]
        public int CategoryID { get; set; }

        [PetaPoco.Column]
        public string QuantityPerUnit { get; set; }

        [PetaPoco.Column]
        public decimal UnitPrice { get; set; }

        [PetaPoco.Column]
        public int UnitsInStock { get; set; }

        [PetaPoco.Column]
        public int UnitsOnOrder { get; set; }

        [PetaPoco.Column]
        public int ReorderLevel { get; set; }

        [PetaPoco.Column]
        public bool Discontinued { get; set; }
       
    }

    public class ProductVIew
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int SupplierID { get; set; }
        public string CompanyName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitsOnOrder { get; set; }
        public int ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

    }
}
