﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NorthWind.Common;
using NorthWind.Entity;
using NorthWind.Facade;
using NorthWind.WebAPI.Models;
using System.Threading.Tasks;

namespace NorthWind.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/employees")]
    public class EmployeesController : ApiController
    {
        EmployeeFacade facade = new EmployeeFacade();

        public async Task<IHttpActionResult> Get()
        {
            var employees = facade.GetAllItems();
            return await Task<IHttpActionResult>.Factory.StartNew(() => (Ok(ApiResponse.OK(employees)))); ;
        }

        public IHttpActionResult Get(string id)
        {
            var employee = facade.GetItemByID(id);
            if (employee == null)
            {
                return Ok(ApiResponse.NotFound("Data not found!"));
            }

            return Ok(ApiResponse.OK(employee));
        }

        public IHttpActionResult Post([FromBody] Employees emp)
        {
            bool status = false;
            string errorMsg = "";
            try
            {
                status = facade.DataInsert(emp, out errorMsg);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ? Ok(ApiResponse.OK("insert data success!")) : Ok(ApiResponse.Error(errorMsg));
        }

        public IHttpActionResult Put([FromBody] Employees emp)
        {
            bool status = false;
            string errorMsg = "";
            try
            {
                status = facade.DataUpdate(emp.EmployeeID, emp, out errorMsg);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ? Ok(ApiResponse.OK("update data success!")) : Ok(ApiResponse.Error(errorMsg));
        }

        public IHttpActionResult Delete(int id)
        {
            bool status = false;
            string errorMsg = "";
            try
            {
                status = facade.DataDelete(id, out errorMsg);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ? Ok(ApiResponse.OK("Delete data success!")) : Ok(ApiResponse.Error(errorMsg));
        }

        [Route("Paging")]
        public IHttpActionResult Paging([FromBody] PageData data)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            foreach (var crt in data.criteria)
            {
                param[crt.criteria] = crt.value;
            }

            var employees = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) / data.pageSize);
            var result = new { rows = employees, rowCount = count, pageCount = pageCount };
            return Ok(new ApiResponse(true, HttpStatusCode.OK, "", result));
        }
    }
}
