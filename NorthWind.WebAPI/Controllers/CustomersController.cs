﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http; 
using NorthWind.Common;
using NorthWind.Entity;
using NorthWind.Facade;
using NorthWind.WebAPI.Models;
using System.Threading.Tasks;

namespace NorthWind.WebAPI.Controllers
{
    [Authorize(Roles ="Admin")]
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiController
    {
        CustomersFacade facade = new CustomersFacade();         

        public async Task<IHttpActionResult> Get()
        { 
            var customers = facade.GetAllItems();
            return await Task<IHttpActionResult>.Factory.StartNew(() => (Ok(ApiResponse.OK(customers))));
        }
         
        public IHttpActionResult Get(string id)
        {
            var customer = facade.GetItemByID(id);
            if (customer == null)
            {
                return Ok(ApiResponse.NotFound("Data not found!"));
            }
            
            return Ok(ApiResponse.OK(customer));
        }

        public IHttpActionResult Post([FromBody] Customers cus)
        {
            bool status = false;
            string errorMsg = "";
            try
            {
                status = facade.DataInsert(cus, out errorMsg);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ? Ok(ApiResponse.OK("insert data success!")) : Ok(ApiResponse.Error(errorMsg));
        }
         
        public IHttpActionResult Put([FromBody] Customers cus)
        {
            bool status = false;
            string errorMsg = "";
            try
            {
                status = facade.DataUpdate(cus.CustomerID, cus, out errorMsg);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ?  Ok(ApiResponse.OK("update data success!")) : Ok(ApiResponse.Error(errorMsg));
        }
         
        public IHttpActionResult Delete(string id)
        {
            bool status = false;
            string errorMsg = "";
            try
            { 
                status = facade.DataDelete(id, out errorMsg);
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
            return status ? Ok(ApiResponse.OK("Delete data success!")) : Ok(ApiResponse.Error(errorMsg));
        }

        [Route("Paging")] 
        public IHttpActionResult Paging([FromBody] PageData data)
        { 
            Dictionary<string, object> param = new Dictionary<string, object>();
            foreach(var crt in data.criteria)
            {
                param[crt.criteria] = crt.value;
            }

            var customers = facade.GetListItemsByLikeCriteria(param, data.page, data.pageSize, data.order);
            var count = facade.GetListItemsByLikeCriteriaCount(param);
            var pageCount = Math.Ceiling(Commons.GetDouble(count) /data.pageSize);
            var result = new { rows = customers, rowCount = count, pageCount = pageCount };
            return Ok(new ApiResponse(true, HttpStatusCode.OK, "", result));
        }
    }
}
