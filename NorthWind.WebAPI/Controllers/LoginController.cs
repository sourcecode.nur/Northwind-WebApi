﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using NorthWind.Common;
using NorthWind.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.IdentityModel.Tokens;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Security;

namespace NorthWind.WebAPI.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {       
        public IHttpActionResult Post([FromBody] dynamic user)
        { 
            if(Request.Headers.Authorization == null)
            {
                return this.BadRequest("Not acceptable request!");
            }
            var auth = Request.Headers.Authorization.ToString().Replace("Basic ", "");
            var byteAuth = TextEncodings.Base64.Decode(auth);
            var strAuth = System.Text.ASCIIEncoding.ASCII.GetString(byteAuth);
            var arrayAuth = strAuth.Split(':');
            var username = arrayAuth[0];
            var password = arrayAuth[1];

            if(!VerifyUserCustom(username, password))
            {
                return this.BadRequest("Invalid login!");
            }
            
            var secret = ConfigurationManager.AppSettings["secret"];
            var keyByteArray = TextEncodings.Base64Url.Decode(secret);
            var securityKey = new InMemorySymmetricSecurityKey(keyByteArray);
            var signature = new SigningCredentials(
                securityKey, 
                SecurityAlgorithms.HmacSha256Signature, 
                SecurityAlgorithms.Sha256Digest);
             
            var jwtHeader = new JwtHeader(signature);
            var issuer = ConfigurationManager.AppSettings["issuer"];
            var audience = ConfigurationManager.AppSettings["audience"];
            var nbf = DateTime.UtcNow;
            var expires = DateTime.UtcNow.AddHours(1);
            var identity = new ClaimsIdentity("JWT");

            identity.AddClaim(new Claim(ClaimTypes.Name, "CODEID"));
            identity.AddClaim(new Claim("sub", "Code Development Indonesia"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Manager"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));

            var payload = new JwtPayload(issuer, audience, identity.Claims, nbf, expires);
            var token = new JwtSecurityToken(jwtHeader, payload);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);
            var result = new { status = true, message = "login success", token = jwt };
            return Ok(result);
        }

        private bool VerifyUserAD(string userName, string password)
        {
            
            DirectoryEntry d;
            DirectorySearcher s;
            d = new DirectoryEntry(ConfigurationManager.AppSettings["ADAddress"], userName, password);
            s = new DirectorySearcher(d);
            s.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(sAMAccountName={0}))", userName);
            try
            {
                SearchResult sr = s.FindOne();
                if (sr != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Startup.LogError(ex.Message);
            }
            return false;
        }

        private bool VerifyUserCustom(string userName, string password)
        {
            if(userName == "admin" && password == "P@ssw0rd")
            {
                return true;
            }
            return false;
        }
    }
}
