﻿using NorthWind.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NorthWind.WebAPI.Controllers
{
    public class DefaultController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok(ApiResponse.OK(new { message = "Welcome to Web API" }));
        }
    }
}
