﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace NorthWind.WebAPI.Models
{
    public class ApiResponse
    {         
        public Meta meta { get; private set; }
        public object data { get; set;  }

        public ApiResponse() { }

        public ApiResponse(Meta meta, object data) { }

        public ApiResponse(bool success, HttpStatusCode status, string message, object data)
        {
            meta = new Meta(success, status, message);
            this.data = data;
        }

        public static ApiResponse Build(bool success, HttpStatusCode status, string message, object data)
        {
            return new ApiResponse(success, status, message, data);
        }

        public static ApiResponse OK(object data)
        {
            var response = new ApiResponse(true, HttpStatusCode.OK, "OK", data);
            return response;
        }

        public static ApiResponse OK(string message)
        { 
            return Build(true, HttpStatusCode.OK, message, null);
        }

        public static ApiResponse OK(string message, object data)
        { 
            return Build(true, HttpStatusCode.OK, message, data);
        }

        public static ApiResponse Created(object data)
        {
            return Build(true, HttpStatusCode.Created, "Created", data); 
        }

        public static ApiResponse Created(string message)
        {
            var response = new ApiResponse(true, HttpStatusCode.Created, message, null);
            return response;
        }

        public static ApiResponse Created(string message, object data)
        {
            return Build(true, HttpStatusCode.Created, message, data); 
        }

        public static ApiResponse Accepted(object data)
        {
            return Build(true, HttpStatusCode.Accepted, "Accepted", data); 
        }

        public static ApiResponse Accepted(string message)
        {
            return Build(true, HttpStatusCode.Accepted, message, null); 
        }

        public static ApiResponse Accepted(string message, object data)
        {
            return Build(true, HttpStatusCode.Accepted, message, data); 
        }

        public static ApiResponse NoContent(object data)
        {
            return Build(true, HttpStatusCode.NoContent, "No Content", data);
        }

        public static ApiResponse NoContent(string message)
        {
            return Build(true, HttpStatusCode.NoContent, message, null);
        }

        public static ApiResponse NoContent(string message, object data)
        {
            return Build(true, HttpStatusCode.NoContent, message, data);
        }

        public static ApiResponse BadRequest(object data)
        {
            return Build(false, HttpStatusCode.BadRequest, "Bad Request", data);
        }

        public static ApiResponse BadRequest(string message)
        {
            return Build(false, HttpStatusCode.BadRequest, message, null);
        }

        public static ApiResponse BadRequest(string message, object data)
        {
            return Build(false, HttpStatusCode.BadRequest, message, data);
        }

        public static ApiResponse Unauthorized(object data)
        {
            return Build(false, HttpStatusCode.Unauthorized, "Unauthorized", data);
        }

        public static ApiResponse Unauthorized(string message)
        {
            return Build(false, HttpStatusCode.Unauthorized, message, null);
        }

        public static ApiResponse Unauthorized(string message, object data)
        {
            return Build(false, HttpStatusCode.Unauthorized, message, data);
        }

        public static ApiResponse Forbidden(object data)
        {
            return Build(false, HttpStatusCode.Forbidden, "Unauthorized", data);
        }

        public static ApiResponse Forbidden(string message)
        {
            return Build(false, HttpStatusCode.Forbidden, message, null);
        }

        public static ApiResponse Forbidden(string message, object data)
        {
            return Build(false, HttpStatusCode.Forbidden, message, data);
        }

        public static ApiResponse NotAcceptable(object data)
        {
            return Build(false, HttpStatusCode.NotAcceptable, "Not Acceptable", data);
        }

        public static ApiResponse NotAcceptable(string message)
        {
            return Build(false, HttpStatusCode.NotAcceptable, message, null);
        }

        public static ApiResponse NotAcceptable(string message, object data)
        {
            return Build(false, HttpStatusCode.NotAcceptable, message, data);
        }

        public static ApiResponse Conflict(object data)
        {
            return Build(false, HttpStatusCode.Conflict, "Conflict", data);
        }

        public static ApiResponse Conflict(string message)
        {
            return Build(false, HttpStatusCode.Conflict, message, null);
        }

        public static ApiResponse Conflict(string message, object data)
        {
            return Build(false, HttpStatusCode.Conflict, message, data);
        }

        public static ApiResponse NotFound(object data)
        {
            return Build(false, HttpStatusCode.NotFound, "Not Found", data);
        }

        public static ApiResponse NotFound(string message)
        {
            return Build(false, HttpStatusCode.NotFound, message, null);
        }

        public static ApiResponse NotFound(string message, object data)
        {
            return Build(false, HttpStatusCode.NotFound, message, data);
        }

        public static ApiResponse NotImplemented(object data)
        {
            return Build(false, HttpStatusCode.NotImplemented, "Not Implemented", data);
        }

        public static ApiResponse NotImplemented(string message)
        {
            return Build(false, HttpStatusCode.NotImplemented, message, null);
        }

        public static ApiResponse NotImplemented(string message, object data)
        {
            return Build(false, HttpStatusCode.NotImplemented, message, data);
        }

        public static ApiResponse InternalServerError(object data)
        {
            return Build(false, HttpStatusCode.InternalServerError, "Internal Server Error", data);
        }

        public static ApiResponse InternalServerError(string message)
        {
            return Build(false, HttpStatusCode.InternalServerError, message, null);
        }

        public static ApiResponse InternalServerError(string message, object data)
        {
            return Build(false, HttpStatusCode.InternalServerError, message, data);
        }

        public static ApiResponse Error(string message)
        {
            return Build(false, HttpStatusCode.NotAcceptable, message, null); 
        } 

        public static ApiResponse InternalServerError(params string[] message)
        {
            return Build(false, HttpStatusCode.InternalServerError, "Internal server error, please contact administrator! Error: " + message, null);           
        }

        public static ApiResponse NotAcceptable(params string[] message)
        {
            return Build(false, HttpStatusCode.NotAcceptable, "Not Acceptable! Error: " + message, null);
        }
    }

    public class Meta
    {
        public bool success { get; private set; }
        public HttpStatusCode status { get; private set; }
        public string message { get; set; }
        public long timespan
        {
            get
            {
                return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            }
        }

        public Meta() { }

        public Meta(bool status, HttpStatusCode statusCode, string message)
        {
            this.success = status;
            this.status = statusCode;
            this.message = message;
        }
    }
}
