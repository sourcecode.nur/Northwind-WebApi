﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NorthWind.WebAPI.Models
{
    public class PageData
    {
        [Required]
        public int page { get; set; }

        [Required]
        public int pageSize { get; set; }

        [Required]
        public Criteria[] criteria { get; set; }

        [Required]
        public string order { get; set; }
    }

    public class Criteria
    {
        [Required]
        public string criteria { get; set; }

        [Required]
        public string value { get; set; }

        public Criteria() { }

        public Criteria(string name, string val) { criteria = name; value = val; }
    } 
}