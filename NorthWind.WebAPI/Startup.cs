﻿using System;
using System.IO;
using System.Web.Http;
using System.Threading.Tasks;
using log4net;
using Owin;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin;
using System.Configuration;
using System.Collections.Generic;

[assembly: OwinStartup(typeof(NorthWind.WebAPI.Startup))]

namespace NorthWind.WebAPI
{
    public partial class Startup
    {
        public static ILog log { get; set; }

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);

            ConfigureJWT(app);
            app.UseWebApi(config);

            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            AUDIENCES = new List<string>();
        } 

        public static void LogInfo(string message)
        {
            log.Info(message);
        }

        public static void LogInfo(string message, Exception ex)
        {
            log.Info(message, ex);
        }

        public static void LogError(string message)
        {
            log.Error(message);
        }

        public static void LogError(string message, Exception ex)
        {
            log.Error(message, ex);
        }
    }
}
