﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace NorthWind.WebAPI
{
    public class AudienceOwinMiddleware : OwinMiddleware
    {
        public AudienceOwinMiddleware(OwinMiddleware next) : base(next)
        {

        }

        public override async Task Invoke(IOwinContext context)
        {
            var response = context.Response;
            var request = context.Request;

            var headers = request.Headers;
            await Next.Invoke(context);
            //await SendError(context);
        }

        private Task SendError(IOwinContext context)
        {
            var response = context.Response; 
            response.WriteAsync("Authorization has been denied for this request.");
            return Task.Factory.StartNew(()=> response);
        }        
    }
}