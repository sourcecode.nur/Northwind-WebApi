﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NorthWind.WebAPI
{
	public partial class Startup
    {
        public static List<string> AUDIENCES;
        public void ConfigureJWT(IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["issuer"];
            var audience = ConfigurationManager.AppSettings["audience"];
            var secret = ConfigurationManager.AppSettings["secret"];
            var key = TextEncodings.Base64Url.Decode(secret); 

            //JWT Bearer Authentication
            app.UseJwtBearerAuthentication(
               new JwtBearerAuthenticationOptions
               {
                   AuthenticationMode = AuthenticationMode.Active,
                   AllowedAudiences = new[] { audience },
                   IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                   {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, key)
                   }
               });

            //Owin MIddleware
            app.Use(typeof(AudienceOwinMiddleware));
        }
    }
}