﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using NorthWind.WebAPI.Filters;
using Newtonsoft.Json.Serialization;

namespace NorthWind.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { controller = "Default", id = RouteParameter.Optional }
            ); 

            //Register Filters
            config.Filters.Add(new InternalServerErrorFilter());
            config.Filters.Add(new ValidateModelAttribute());

            //Register Json Formatters
            config.Formatters.Add(new JsonFormatter());
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            ((DefaultContractResolver)config.Formatters.JsonFormatter.SerializerSettings.ContractResolver).IgnoreSerializableAttribute = true;

            //Enable CORS
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MessageHandlers.Add(new PreflightRequestsHandler());
        }
    }
}
