﻿using Newtonsoft.Json; 
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using NorthWind.WebAPI.Models;

namespace NorthWind.WebAPI.Filters
{
    public class InternalServerErrorFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception != null)
                Startup.LogError(context.Exception.Message, context.Exception);
            if (context.Response == null)
                 context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, ApiResponse.InternalServerError());
            if (context.Response.StatusCode == HttpStatusCode.InternalServerError)
                context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, ApiResponse.InternalServerError(context.Exception.Message));
            else
                base.OnException(context);
        }

        public override Task OnExceptionAsync(HttpActionExecutedContext context, CancellationToken cancellationToken)
        {
            if (context.Exception != null)
                Startup.LogError(context.Exception.Message, context.Exception);
            if (context.Response == null)
                context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, ApiResponse.InternalServerError());
            if (context.Response.StatusCode == HttpStatusCode.InternalServerError)
                context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, ApiResponse.InternalServerError(context.Exception.Message));
            else
                base.OnException(context);
            return Task.FromResult<object>(null);
        }
    }
}