﻿namespace NorthWind.WebMVC
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Globalization;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Serialization;

    public static class HtmlHelperExtensions
    {
       public static HtmlString IncudeNgon(
           this HtmlHelper t,
           String jsNamespace = @"ngon",
           Boolean outputScriptTag = true)
        {
            var viewData = t.ViewContext.ViewData;

            if (viewData == null)
            {
                return MvcHtmlString.Empty;
            }

            var ngon = viewData[@"Ngon"] as ExpandoObject;

            if (ngon == null)
            {
                throw new InvalidOperationException(@"Canot find NGon");
            }

            var jsonSetting = 
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new JsonConverter[] { new StringEnumConverter() }
                };

            var builder =
                new StringBuilder(
                    String.Format(@"window.{0}={{}};", jsNamespace));

            foreach(var prop in ngon)
            {
                builder.AppendFormat(
                    @"{0}.{1}={2};",
                    jsNamespace,
                    ToJavaScriptCamelCase(prop.Key),
                    t.Raw(JsonConvert.SerializeObject(prop.Value, jsonSetting)));
            }

            var outputJs = builder.ToString();

            if (!outputScriptTag)
            {
                return new HtmlString(outputJs);
            }

            var tag = new TagBuilder(@"script");
            tag.Attributes.Add(new KeyValuePair<String, String>(@"type", @"type/javascript"));
            tag.InnerHtml = outputJs;

            return new HtmlString(tag.ToString());
        }

        private static String ToJavaScriptCamelCase(
            String extenString)
        {
            if (String.IsNullOrEmpty(extenString))
            {
                return extenString;
            }

            if (!Char.IsUpper(extenString[0]))
            {
                return extenString;
            }

            var sb = new StringBuilder();

            for(var i = 0; i < extenString.Length; i++)
            {
                var hasNext =
                    i + 1 < extenString.Length;

                if ((i == 0 ||
                    !hasNext) ||
                    Char.IsUpper(extenString[i + 1]))
                {
                    sb.Append(Char.ToLower(extenString[i], CultureInfo.InvariantCulture));
                }
                else
                {
                    sb.Append(extenString.Substring(i));
                    break;
                }
            }

            return sb.ToString();
        }
    }
}
