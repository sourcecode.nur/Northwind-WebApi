﻿namespace NorthWind.WebMVC
{
    using System.Dynamic;
    using System.Web.Mvc;

    public class NgonActionFilterAttribute
        : ActionFilterAttribute
    {
        public override void OnActionExecuting(
            ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.Ngon = new ExpandoObject();
            base.OnActionExecuting(filterContext);
        }
    }
}
