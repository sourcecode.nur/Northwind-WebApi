﻿
$.fn.digits = function () {
    return this.each(function () {
        $(this).val($(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $(this).keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $(this).css("text-align", "right");
        $(this).focusout(function () { $(this).val($(this).val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); });
    })
}

$.fn.freeze = function () {
    return this.each(function () {
        $(this).keydown(function () { return false; });
    });
}

$.fn.initdate = function () {
    var option = { format: 'd-M-yyyy', autoclose: true, enableOnReadonly: false };
    this.each(function (i, e) {
        var inp = $(e).find("input");
        var btn = $(e).find(".input-group-addon");
        var eli = $(e).find(".input-group-addon i");
        if ($(inp).attr("readonly") == "readonly" || $(inp).attr("disabled") == "disabled") {
            $(btn).css("background", "#EEE");
            $(inp).css("cursor", "not-allowed");
            $(btn).css("cursor", "not-allowed");
            $(eli).css("cursor", "not-allowed");
        } else {
            $(btn).css("cursor", "pointer");
            $(btn).click(function () {
                $(inp).datepicker(option).on("hide", function () {
                    $(inp).datepicker("remove");
                }).datepicker("show");
            });
            $(inp).on("keydown", function (evt) { return false; });
            $(inp).change(function (a) {
                if ($(inp).parent().parent().parent().hasClass("has-error")) {
                    $(this).parent().parent().parent().removeClass("has-error");
                }
            });
        }
    });
}

function setDatePicker() {
    var option = { format: 'd-M-yyyy', autoclose: true, enableOnReadonly: false };
    $(".input-group.date").each(function (i, e) {
        var inp = $(e).find("input");
        var btn = $(e).find(".input-group-addon");
        var eli = $(e).find(".input-group-addon i");
        if ($(inp).attr("readonly") == "readonly" || $(inp).attr("disabled") == "disabled") {
            $(btn).css("background", "#EEE");
            $(inp).css("cursor", "not-allowed");
            $(btn).css("cursor", "not-allowed");
            $(eli).css("cursor", "not-allowed");
        } else {
            $(btn).css("cursor", "pointer");
            $(btn).click(function () {
                $(inp).datepicker(option).on("hide", function () {
                    $(inp).datepicker("remove");
                }).datepicker("show");
            });
            $(inp).on("keydown", function (evt) { return false; });
            $(inp).change(function (a) {
                if ($(inp).parent().parent().parent().hasClass("has-error")) {
                    $(this).parent().parent().parent().removeClass("has-error");
                }
            });
        } 
    });
}