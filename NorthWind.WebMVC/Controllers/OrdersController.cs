﻿using DataTables.Mvc;
using NorthWind.Common;
using NorthWind.Entity;
using NorthWind.Facade;
using NorthWind.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NorthWind.WebMVC.Controllers
{
    [Authorize] 
    [RoutePrefix("Orders")]
    public class OrdersController : Controller
    {
        private List<OrderDetailMap> ListOrderDetail
        {
            get
            {
                if (Session["LIST_DETAIL"] == null)
                {
                    return new List<OrderDetailMap>();
                }
                return Session["LIST_DETAIL"] as List<OrderDetailMap>;
            }
            set { Session["LIST_DETAIL"] = value; }
        }

        // GET: Orders
        public ActionResult Index()
        {
            return View();
        }

        // GET: Orders/Details/5
        public ActionResult Details(int id)
        {
            var facade = new OrderFacade();
            var model = facade.GetOrderModel(id); 
            ListOrderDetail = model.ListOrderDetail;
            return PartialView("_Details", model);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            var model = new OrderViewModel();
            model.Order = new Order();
            model.Customer = new Customers();
            model.Shipper = new Shippers();
            model.Employee = new Employees();
            ListOrderDetail.Clear();
            return PartialView("_Create", model);
        }

        // POST: Orders/Create
        [HttpPost]
        public async Task<ActionResult> Create(OrderViewModel model)
        {
            string errMsg = string.Empty;
            bool status = false;
            try
            {
                var facade = new OrderFacade();
                var dFacade = new OrderDetailFacade();
                facade.DataInsert(model.Order, ListOrderDetail, out errMsg);
                status = string.IsNullOrEmpty(errMsg);

                await Task<bool>.Factory.StartNew(() => status);
                if (!status)
                {
                    ModelState.AddModelError("", "Insert data error \n, Error: " + errMsg);
                    return PartialView("_Create", model);
                }
                if (Request.IsAjaxRequest())
                {
                    ListOrderDetail.Clear();
                    return Content("success");
                }
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int id)
        {
            var facade = new OrderFacade();
            var model = facade.GetOrderModel(id);
            ListOrderDetail = model.ListOrderDetail;
            return PartialView("_Edit", model);
        }

        // POST: Orders/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(OrderViewModel model)
        {
            string errMsg = string.Empty;
            bool status = false;
            try
            {
                var facade = new OrderFacade();
                var dFacade = new OrderDetailFacade();
                facade.DataUpdate(model.Order, ListOrderDetail, out errMsg);
                status = string.IsNullOrEmpty(errMsg);

                await Task<bool>.Factory.StartNew(() => status);
                if (!status)
                {
                    ModelState.AddModelError("", "Update data error \n, Error: " + errMsg);
                    return PartialView("_Edit", model);
                }
                if (Request.IsAjaxRequest())
                {
                    ListOrderDetail.Clear();
                    return Content("success");
                }
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Orders/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }        

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest req)
        {
            var facade = new OrderFacade();
            var sortedCol = req.Columns.GetSortedColumns();
            var strOrder = string.Empty;
            foreach (var col in sortedCol)
            {
                strOrder += string.IsNullOrEmpty(strOrder) ? "" : ",";
                strOrder += col.Data + (col.SortDirection == Column.OrderDirection.Ascendant ? " ASC" : " DESC");
            }

            var totalCount = facade.GetListItemsByCriteriaCount("1=1");
            var start = req.Start / req.Length + 1;
            var crtVal = req.Search.Value.Trim();
            var page = facade.PageDataForDataTables(crtVal, start, req.Length, strOrder); 
            var result = new DataTablesResponse(req.Draw, page.Items, (int)page.TotalItems, totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrderDetail([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest req)
        {
            var sortedCol = req.Columns.GetSortedColumns();
            var strOrder = string.Empty;
            foreach (var col in sortedCol)
            {
                strOrder += string.IsNullOrEmpty(strOrder) ? "" : ",";
                strOrder += col.Data + (col.SortDirection == Column.OrderDirection.Ascendant ? " ASC" : " DESC");
            }
            var items = ListOrderDetail.OrderBy(strOrder).ToList();
            var strVal = req.Search.Value.Trim(); 
            if (!string.IsNullOrEmpty(strVal))
            {
                items = ListOrderDetail.Where("ProductID.ToString().Contains(@0) OR ProductName.Contains(@0)", strVal).ToList();
            } 
            var totalCount = ListOrderDetail.Count; 
             
            var result = new DataTablesResponse(req.Draw, items.Skip(req.Start).Take(req.Length).ToList(), items.Count, totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
        [HttpGet] 
        public ActionResult OrderDetail(int id, int state)
        {
            var orderdetail = ListOrderDetail.Where(x => x.ProductID == id).SingleOrDefault();
            string view = state == 1 ? "_OrderDetailView" : "_OrderDetailCreateEdit";

            return PartialView(view, orderdetail);
        }

        [HttpGet]
        public ActionResult AddOrderDetail(int id)
        {
            var orderdetail = new OrderDetailMap();
            orderdetail.OrderID = id; 
            return PartialView("_OrderDetailCreateEdit", orderdetail);
        }

        // POST: Orders/DeleteOrderDetail/5 
        [HttpPost] 
        public string DeleteOrderDetail(int id)
        {
            var orderdetail = ListOrderDetail.Where(x => x.ProductID == id).SingleOrDefault();
            if(orderdetail == null)
            {
                return "Data not found!";
            }
            else
            {
                ListOrderDetail.Remove(orderdetail);
                return "Success";
            }

        }

        [HttpPost]
        public async Task<ActionResult> AddEditOrderDetail(OrderDetailMap OrderDetail)
        {
            string errMsg = string.Empty;
            bool status = false;
            try
            {
                var od = ListOrderDetail.Where(x => x.ProductID == OrderDetail.ProductID).SingleOrDefault();
                if(od == null)
                {
                    ListOrderDetail.Add(OrderDetail);
                    status = true;
                }
                else
                {
                    int index = ListOrderDetail.IndexOf(od);
                    ListOrderDetail.RemoveAt(index);
                    ListOrderDetail.Insert(index, OrderDetail);
                    status = true;
                }
                await Task<bool>.Factory.StartNew(() => status);
                if (!status)
                {
                    ModelState.AddModelError("", "Update data error \n, Error: " + errMsg);
                    return PartialView("_OrderDetailCreateEdit", OrderDetail);
                }
                if (Request.IsAjaxRequest())
                {
                    return Content("success");
                }
            }
            catch
            {
                return View();
            }
            return RedirectToAction("Index");
        }
    }
}
