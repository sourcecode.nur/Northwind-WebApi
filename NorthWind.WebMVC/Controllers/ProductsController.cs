﻿using DataTables.Mvc;
using NorthWind.Entity;
using NorthWind.Facade;
using NorthWind.WebMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NorthWind.WebMVC.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            var facade = new ProductsFacade();
            var obj = facade.GetItemByID(id);
            var ctg = new CategoryFacade().GetItemByID(obj.CategoryID);
            var supp = new SupplierFacade().GetItemByID(obj.SupplierID);
            var model = new ProductViewModel() { State=1, Products = obj, Category = ctg, Supplier = supp};
            return PartialView("_Details", model);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            var obj = new Products();
            var ctg = new Categories();
            var supp = new Suppliers();
            var model = new ProductViewModel { State = 0, Products = obj , Category = ctg, Supplier = supp };
            return PartialView("_Create", model);
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int id)
        {
            var facade = new ProductsFacade();
            var obj = facade.GetItemByID(id);
            var ctg = new CategoryFacade().GetItemByID(obj.CategoryID);
            var supp = new SupplierFacade().GetItemByID(obj.SupplierID);
            var list = new CategoryFacade().GetAllItems();
            var model = new ProductViewModel() { State = 2, Products = obj, Category = ctg, Supplier = supp, CategoryList = list };
            return PartialView("_Edit", model);
        }

        
        [HttpPost]
        public async Task<ActionResult> Edit(ProductViewModel model)
        {
            string errMsg = string.Empty;
            try
            {
                var facade = new ProductsFacade();
                bool status = facade.DataUpdate(model.Products.ProductID, model.Products, out errMsg);
                await Task<bool>.Factory.StartNew(() => status);
                if (!status)
                {
                    ModelState.AddModelError("", "Update data error \n, Error: " + errMsg);
                    return View("_Edit", model);
                }
                if (Request.IsAjaxRequest())
                {
                    return Content("success");
                }
            }
            catch
            {
                return View();
            }
            
            return RedirectToAction("Index");
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest req)
        {
            var facade = new ProductsFacade();
            var sortedCol = req.Columns.GetSortedColumns();
            var strOrder = string.Empty;
            foreach (var col in sortedCol)
            {
                strOrder += string.IsNullOrEmpty(strOrder) ? "" : ",";
                strOrder += col.Data + (col.SortDirection == Column.OrderDirection.Ascendant ? " ASC" : " DESC");
            }

            var totalCount = facade.GetListItemsByCriteriaCount("1=1");
            var start = req.Start / req.Length + 1;
            var crtVal = req.Search.Value.Trim(); 
            var page = facade.PageDataForDataTables(crtVal, start, req.Length, strOrder);
            var result = new DataTablesResponse(req.Draw, page.Items, (int)page.TotalItems, totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Lookup()
        {
            return PartialView("_Lookup");
        }
    }
}
