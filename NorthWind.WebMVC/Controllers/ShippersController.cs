﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTables.Mvc;
using NorthWind.Facade;

namespace NorthWind.WebMVC.Controllers
{
    [Authorize]
    public class ShippersController : Controller
    {
        // GET: Shippers
        public ActionResult Index()
        {
            return View();
        }

        // GET: Shippers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Shippers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shippers/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Shippers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Shippers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Shippers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Shippers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest req)
        {
            var facade = new ShipperFacade();
            var sortedCol = req.Columns.GetSortedColumns();
            var strOrder = string.Empty;
            foreach (var col in sortedCol)
            {
                strOrder += string.IsNullOrEmpty(strOrder) ? "" : ",";
                strOrder += col.Data + (col.SortDirection == Column.OrderDirection.Ascendant ? " ASC" : " DESC");
            }

            var totalCount = facade.GetListItemsByCriteriaCount("1=1");
            var start = req.Start / req.Length + 1;
            var crtVal = req.Search.Value.Trim();
            var page = facade.PageDataForDataTables(crtVal, start, req.Length, strOrder);

            var result = new DataTablesResponse(req.Draw, page.Items, (int)page.TotalItems, totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Lookup()
        {
            return PartialView("_Lookup");
        }
    }
}
