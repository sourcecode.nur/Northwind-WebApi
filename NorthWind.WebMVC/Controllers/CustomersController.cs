﻿namespace NorthWind.WebMVC.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using DataTables.Mvc;
    using NorthWind.Facade;
    using Newtonsoft.Json.Linq;

    using Common;
    using Entity;

    [Authorize]
    public class CustomersController
        : Controller
    {
        // GET: Customers
        public ActionResult Index()
        {
            //var actionName = this.ControllerContext.RouteData.Values[@"action"].ToString();
            var controllerName = this.ControllerContext.RouteData.Values[@"controller"].ToString();

            var jsFunc = @" function (data, type, full, meta) { return '<a href=""javascript:showForm(0,\'' + data + '\')"" style=""margin-right: 5px""><i class=""glyphicon glyphicon-search""></i></a>' + 
                    '<a href=""javascript:showForm(2,\'' + data + '\')"" style=""color: green; margin-right: 5px""><i class=""glyphicon glyphicon-edit""></i></a>' +
                    '<a href=""javascript:showForm(3,\'' + data + '\')"" style=""color: red; margin-right: 5px""><i class=""glyphicon glyphicon-remove""></i></a>'
                }";

            var customer = new Customers();

            var columns = new Object[]
            {
                new { Data = "CustomerID", Sortable = false, Width = 80, Render = new JRaw(jsFunc) },
                new { Title = "Customer ID", Data = Commons.GetPropertyInfo(customer, p => p.CustomerID).Name },
                new { Title = "Company Name", Data = Commons.GetPropertyInfo(customer, p => p.CompanyName).Name },
                new { Title = "Contact Name", Data = Commons.GetPropertyInfo(customer, p => p.ContactName).Name },
                new { Title = "Contact Title", Data = Commons.GetPropertyInfo(customer, p => p.ContactTitle).Name },
                new { Title = "Address", Data = Commons.GetPropertyInfo(customer, p => p.Address).Name },
                new { Title = "Country", Data = Commons.GetPropertyName(() => customer.Country) }
            };

            var lengthMenu = new Int32[][]
            {
                new Int32[] { 10, 25, 50, 100 },
                new Int32[] { 10, 25, 50, 100 }
            };

            var aaSorting = new[]
            {
                new Object[] { 1, "asc" }
            };

            var datatableConfig =
                new
                {
                    ServerSide = true,
                    Processing = true,
                    ScrollX = true,
                    Lengthmenu = lengthMenu,
                    AaSorting = aaSorting,
                    Ajax = new { Url = Url.Action(@"Get", controllerName) },
                    Columns = columns
                };
            
            this.ViewBag.Ngon.Dtc = datatableConfig;

            this.ViewBag.Ngon.Dataku = 
                new 
                {
                    TableId    = @"customers-dt",
                    LengthMenu = lengthMenu,
                    UrlAdd  = Url.Action(@"Create", controllerName).Substring(1),
                    UrlDetails = Url.Action(@"Details", controllerName).Substring(1),
                    UrlEdit = Url.Action(@"Edit", controllerName).Substring(1),
                    UrlDelete = Url.Action(@"Delete", controllerName).Substring(1)
                };

            return View();
        }

        // GET: Customers/Details/5
        public ActionResult Details(
            String Id)
            //Int32 id)
        {
            return this.PartialView(
                "_Details", 
                new Models.ProductViewModel());
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return this.PartialView(
                @"_Add",
                new Models.ProductViewModel());
        }

        // POST: Customers/Create
        [HttpPost]
        public ActionResult Create(
            FormCollection collection)
        {
            if (this.Request.IsAjaxRequest())
            {
                return this.Content(@"success");
            }

            return this.RedirectToAction(@"Index");
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(
            String Id)
            //Int32 id)
        {
            return PartialView(
                @"_Edit", 
                new Models.ProductViewModel());
        }

        // POST: Customers/Edit/5
        [HttpPost]
        public ActionResult Edit(
            String Id,
            //Int32 id, 
            FormCollection collection)
        {
            if (this.Request.IsAjaxRequest())
            {
                return this.Content(@"success");
            }

            return this.RedirectToAction(@"Index");
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(
            String Id)
            //Int32 id)
        {
            return this.PartialView(
                @"_Delete",
                new Models.ProductViewModel());
        }

        // POST: Customers/Delete/5
        [HttpPost]
        public ActionResult Delete(
            String Id,
            //int id, 
            FormCollection collection)
        {
            if (this.Request.IsAjaxRequest())
            {
                return this.Content(@"success");
            }

            return this.RedirectToAction(@"Index");
        }

        public ActionResult Get([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest req)
        {
            var facade = new CustomersFacade();
            var sortedCol = req.Columns.GetSortedColumns();
            var strOrder = string.Empty;
            foreach (var col in sortedCol)
            {
                strOrder += string.IsNullOrEmpty(strOrder) ? "" : ",";
                strOrder += col.Data + (col.SortDirection == Column.OrderDirection.Ascendant ? " ASC" : " DESC");
            }

            var totalCount = facade.GetListItemsByCriteriaCount("1=1");
            var start = req.Start / req.Length + 1;
            var crtVal = req.Search.Value.Trim();
            var page = facade.PageDataForDataTables(crtVal, start, req.Length, strOrder);

            var result = new DataTablesResponse(req.Draw, page.Items, (int)page.TotalItems, totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Lookup()
        {
            return this.PartialView("_Lookup");
        }
    }
}
