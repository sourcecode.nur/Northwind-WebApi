﻿using NorthWind.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthWind.WebMVC.Models
{
    public class ProductViewModel
    {
        public int State { get; set; }

        public Products Products { get; set; }

        public Categories Category { get; set; }

        public Suppliers Supplier { get; set; }

        public List<Categories> CategoryList { get; set; }

    }
}