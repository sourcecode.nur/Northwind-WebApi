﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NorthWind.Entity;

namespace NorthWind.WebMVC.Models
{
    public class OrderViewModel
    {
        public Order Order { get; set; }

        public Customers Customer { get; set; }

        public Employees Employee { get; set; }

        public Shippers Shipper { get; set; }

        public List<OrderDetailMap> ListOrderDetail { get; set; }
    }
}